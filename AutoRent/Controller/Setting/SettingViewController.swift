//
//  SettingViewController.swift
//  AutoRent
//
//  Created by mac on 4/30/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func back(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container")
        self.navigationController?.present(linkingVC, animated: true, completion: nil)
        //  pushViewController(linkingVC, animated: true)
        
    }

}
