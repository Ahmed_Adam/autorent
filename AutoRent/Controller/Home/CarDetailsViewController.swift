//
//  CarDetailsViewController.swift
//  AutoRent
//
//  Created by mac on 4/24/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class CarDetailsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func Extra(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Extra", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "ExtraViewController")
        self.navigationController?.pushViewController(linkingVC, animated: true)
    }
    
    @IBAction func onMoreTapped() {
        print("TOGGLE SIDE MENU")
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        
    }


}
