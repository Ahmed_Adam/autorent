//
//  CarSearchViewController.swift
//  AutoRent
//
//  Created by mac on 4/23/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class CarSearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
   
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let CarCompany = self.storyboard?.instantiateViewController(withIdentifier: "CarCompanyViewController") 
        self.show(CarCompany!, sender: self)
     //   self.present(CarCompany, animated: true, completion: nil)
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell =  tableView.dequeueReusableCell(withIdentifier: "carCell", for: indexPath)
        
        return cell
    }

}
