//
//  CarDetailsViewController.swift
//  AutoRent
//
//  Created by mac on 4/24/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit
import SnapKit

class CarCompanyViewController: UIViewController , UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
//        let logo = #imageLiteral(resourceName: "Europcar-Logo.svg")
//        let imageView = UIImageView(image:logo)
////        imageView.frame.size.width = 100
//        imageView.snp.makeConstraints { (make) in
//            make.width.equalTo(100)
//        }
//        self.navigationItem.titleView = imageView
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let CarDetails = self.storyboard?.instantiateViewController(withIdentifier: "CarDetailsViewController")
        self.show(CarDetails!, sender: self)
    }
    
   
    
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "carDetailsCell", for: indexPath)
        
        return cell
    }
    
}
