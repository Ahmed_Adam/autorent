//
//  MainVC.swift
//  Side Menu
//
//  Created by Kyle Lee on 8/6/17.
//  Copyright © 2017 Kyle Lee. All rights reserved.
//

import UIKit
import DatePickerDialog
import DropDown

class HomeVC: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "carTypeCell", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.carTypeButtonOutLet.setTitle("  Small car", for: .normal)
        carTypeDropDownList.isHidden = true
        self.CarYypeButtonArrow.image = #imageLiteral(resourceName: "arrowRight120x120-10")
        
        
    }
    

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var carTypeDropDownList: UIView!
    @IBOutlet weak var carTypeViewconstraints: NSLayoutConstraint!
    
    @IBOutlet weak var carTypeView: UIView!
    
    @IBOutlet weak var customTypeViewconstraints: NSLayoutConstraint!
    
    @IBOutlet weak var customTypeView: UIView!
    
    @IBOutlet weak var CarYypeButtonArrow: UIImageView!
    @IBOutlet weak var CusuomYypeButtonArrow: UIImageView!

    @IBOutlet weak var SwitchB: UISwitch!
    
    @IBOutlet weak var carTypeButtonOutLet: UIButton!
    @IBOutlet weak var CustomTypeButtonLayout: UIButton!
    @IBOutlet weak var dayTextField: UITextField!
    @IBOutlet weak var monthTextField: UITextField!
    @IBOutlet weak var dayNamae: UITextField!
    @IBOutlet weak var timeTextFiled: UITextField!
    
    @IBOutlet weak var dayTextField_f: UITextField!
    @IBOutlet weak var monthTextField_f: UITextField!
    @IBOutlet weak var dayNamae_f: UITextField!
    @IBOutlet weak var timeTextFiled_f: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dayTextField.delegate = self
        monthTextField.delegate = self
        dayNamae.delegate = self
        timeTextFiled.delegate = self
        dayTextField_f.delegate = self
        monthTextField_f.delegate = self
        dayNamae_f.delegate = self
        timeTextFiled_f.delegate = self
        carTypeDropDownList.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        
    //    carTypeViewconstraints.constant = 0
        customTypeViewconstraints.constant = 0
        SwitchB.layer.borderWidth = 1
        SwitchB.layer.borderColor = UIColor.lightGray.cgColor
        
        carTypeButtonOutLet.layer.borderWidth = 1
        carTypeButtonOutLet.layer.borderColor = UIColor.lightGray.cgColor
        CustomTypeButtonLayout.layer.borderWidth = 1
        CustomTypeButtonLayout.layer.borderColor = UIColor.lightGray.cgColor
    
        }
    
    
    func datePickerTapped() {

        
        let datePicker = DatePickerDialog(textColor: .black,
                                          buttonColor: .black,
                                          font: UIFont.boldSystemFont(ofSize: 17),
                                          showCancelButton: true)
        datePicker.show("Choose a date",
                        doneButtonTitle: "Done",
                        cancelButtonTitle: "Cancel",
                        datePickerMode: .dateAndTime) { (date) in
                            if let dt = date {
                                let formatter = DateFormatter()
                             //   formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                formatter.dateFormat = "dd"
                                self.dayTextField.text = formatter.string(from: dt)
                                formatter.dateFormat = "MMM"
                                self.monthTextField.text = formatter.string(from: dt)
                                formatter.dateFormat = "E"
                                self.dayNamae.text = formatter.string(from: dt)
                                formatter.dateFormat = "h:mm a"
                                self.timeTextFiled.text = formatter.string(from: dt)
                                
                                
                            }
        }
    }
    func datePickerTapped_F() {
        
        
        let datePicker = DatePickerDialog(textColor: .black,
                                          buttonColor: .black,
                                          font: UIFont.boldSystemFont(ofSize: 17),
                                          showCancelButton: true)
        datePicker.show("DatePickerDialog",
                        doneButtonTitle: "Done",
                        cancelButtonTitle: "Cancel",
                        datePickerMode: .dateAndTime) { (date) in
                            if let dt = date {
                                let formatter = DateFormatter()
                                //   formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                formatter.dateFormat = "dd"
                                self.dayTextField_f.text = formatter.string(from: dt)
                                formatter.dateFormat = "MMM"
                                self.monthTextField_f.text = formatter.string(from: dt)
                                formatter.dateFormat = "E"
                                self.dayNamae_f.text = formatter.string(from: dt)
                                formatter.dateFormat = "h:mm a"
                                self.timeTextFiled_f.text = formatter.string(from: dt)
                                
                                
                            }
        }
    }
    
    @IBAction func selectCar(_ sender: UIButton) {
        
        
        let dropDownList = DropDown()
        dropDownList.anchorView = (sender as AnchorView)
//        dropDownList.dataSource = countryNames
        dropDownList.addSubview(carTypeView)
        dropDownList.bottomOffset = CGPoint(x: 0, y:(dropDownList.anchorView?.plainView.bounds.height)!)
        dropDownList.selectionAction = {(index : Int , item : String)in
            sender.setTitle(item, for: .normal)
            
            
        }
        dropDownList.show()
        
    }
 
    
        
    @IBAction func SelcetCarType(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            carTypeDropDownList.isHidden = false
            self.CarYypeButtonArrow.image = #imageLiteral(resourceName: "d120x120")
            self.carTypeButtonOutLet.setTitle("  Car Type", for: .normal)
            
            carTypeDropDownList.addSubview(carTypeView)
//            self.carTypeButtonOutLet.backgroundColor = UIColor.white
//            self.carTypeButtonOutLet.titleColor(for: .normal) = UIColor.black
        }
        else {
            carTypeDropDownList.isHidden = true
            self.CarYypeButtonArrow.image = #imageLiteral(resourceName: "arrowRight120x120-10")
        }
        
        
        
//        UIView.animate(withDuration: 0.25) {
//            self.carTypeViewconstraints.constant = sender.isSelected ? 77 : 0
//
//            self.view.layoutIfNeeded()
 //       }

        
    }
    @IBAction func SelcetCustomType(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            self.CusuomYypeButtonArrow.image = #imageLiteral(resourceName: "d120x120")
             self.CustomTypeButtonLayout.setTitle("  Custom Type", for: .normal)
        }
        else {
            self.CusuomYypeButtonArrow.image = #imageLiteral(resourceName: "arrowRight120x120-10")
        }
        UIView.animate(withDuration: 0.25) {
            self.customTypeViewconstraints.constant = sender.isSelected ? 77 : 0
           
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func SUVchoice(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.25) {
            self.carTypeViewconstraints.constant = 0
             self.CarYypeButtonArrow.image = #imageLiteral(resourceName: "arrowRight120x120-10")
            self.carTypeButtonOutLet.isSelected = false
            self.carTypeButtonOutLet.setTitle("  SUV", for: .normal)
            self.view.layoutIfNeeded()
        }
        
        
        

    }
    
    @IBAction func smallCarCoice(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.25) {
            self.CarYypeButtonArrow.image = #imageLiteral(resourceName: "arrowRight120x120-10")
            self.carTypeButtonOutLet.isSelected = false
            self.carTypeButtonOutLet.setTitle("  Small Car", for: .normal)
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func PersonalCoice(_ sender: UIButton) {
        UIView.animate(withDuration: 0.25) {
            self.customTypeViewconstraints.constant = 0
            self.CusuomYypeButtonArrow.image = #imageLiteral(resourceName: "arrowRight120x120-10")
            self.CustomTypeButtonLayout.isSelected = false
            self.CustomTypeButtonLayout.setTitle("  Personal", for: .normal)
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func companyChoice(_ sender: Any) {
        UIView.animate(withDuration: 0.25) {
            self.customTypeViewconstraints.constant = 0
            self.CusuomYypeButtonArrow.image = #imageLiteral(resourceName: "arrowRight120x120-10")
            self.CustomTypeButtonLayout.isSelected = false
            self.CustomTypeButtonLayout.setTitle("  Company", for: .normal)
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func onMoreTapped() {
        print("TOGGLE SIDE MENU")
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        
    }
    
    

}


extension HomeVC: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.dayTextField || textField == self.monthTextField || textField == self.dayNamae || textField == self.timeTextFiled
            {
            datePickerTapped()
            return false
        }
      else  if textField == self.dayTextField_f || textField == self.monthTextField_f || textField == self.dayNamae || textField == self.timeTextFiled_f
        {
            datePickerTapped_F()
            return false
        }
        
        return true
    }
}
