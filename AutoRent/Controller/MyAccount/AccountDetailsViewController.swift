//
//  AccountDetailsViewController.swift
//  AutoRent
//
//  Created by mac on 4/26/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit
import DropDown

class AccountDetailsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
let status = ["Mr","Mis"]

    @IBAction func selectCountry(_ sender: UIButton) {
        
        
        let dd = DropDown()
        dd.anchorView = (sender as AnchorView)
        dd.dataSource = status
        dd.bottomOffset = CGPoint(x: 0, y:(dd.anchorView?.plainView.bounds.height)!)
        dd.selectionAction = {(index : Int , item : String)in
            sender.setTitle(item, for: .normal)
  
            
        }
        dd.show()
        
    }

}
