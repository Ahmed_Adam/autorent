//
//  MyBookingsViewController.swift
//  AutoRent
//
//  Created by mac on 4/26/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class MyBookingsViewController: UIViewController {

    @IBOutlet weak var goToBook: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        goToBook.layer.borderWidth = 2
        goToBook.layer.borderColor = UIColor.orange.cgColor
    }
    
    @IBAction func back(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container")
        self.navigationController?.present(linkingVC, animated: true, completion: nil)
        
    }


}
