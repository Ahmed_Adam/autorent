//
//  LogInViewController.swift
//  AutoRent
//
//  Created by mac on 4/29/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController , UITextFieldDelegate{

    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var userNameView: UIView!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        userNameView.layer.borderWidth = 2
        userNameView.layer.borderColor = UIColor.orange.cgColor
        passwordView.layer.borderWidth = 2
        passwordView.layer.borderColor = UIColor.orange.cgColor
        userNameTF.delegate = self
        passwordTF.delegate = self
        
    }
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    
    @IBAction func onMoreTapped() {
        print("TOGGLE SIDE MENU")
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        
    }


}
