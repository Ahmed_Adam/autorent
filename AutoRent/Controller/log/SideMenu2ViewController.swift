//
//  SideMenu2ViewController.swift
//  AutoRent
//
//  Created by mac on 4/30/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit



    
    class SideMenu2ViewController: UITableViewController{
        
        
        
        
        @IBAction func HomeButton(_ sender: Any) {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container")
            self.present(linkingVC, animated: true, completion: nil)
            
        }
        
        
        
        //    let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //    let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container2")
        //    self.navigationController?.present(linkingVC, animated: true, completion: nil)
        
        @IBAction func MyAccButton(_ sender: Any) {
            
            let storyboard = UIStoryboard(name: "MyAccount", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "MyAccount")
            self.present(linkingVC, animated: true, completion: nil)
            
        }
        
        @IBAction func MyBookingsButton(_ sender: Any) {
            
            let storyboard = UIStoryboard(name: "MyBooking", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "MyBookings")
            self.present(linkingVC, animated: true, completion: nil)
            
        }
        
        @IBAction func wishList(_ sender: Any) {
            let storyboard = UIStoryboard(name: "WishList", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "MyWishList")
            self.present(linkingVC, animated: true, completion: nil)
            
        }
        
        @IBAction func logIn(_ sender: Any) {
            
            let home = self.storyboard?.instantiateViewController(withIdentifier: "Container")
            self.present(home!, animated: true, completion: nil)
        }
        
        @IBAction func NotificationButton(_ sender: UIButton) {
            
            //        let myNoti = self.storyboard?.instantiateViewController(withIdentifier: "MyNotification") as! NotificationVC
            //        self.present(myNoti, animated: true, completion: nil)
        }
        
        @IBAction func ContactUsButton(_ sender: Any) {
            
            
        }
        
        
        @IBAction func setting(_ sender: Any) {
            
            let storyboard = UIStoryboard(name: "Setting", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "Setting")
            self.present(linkingVC, animated: true, completion: nil)
        }
        
        @IBAction func privacyButton(_ sender: UIButton) {
            let storyboard = UIStoryboard(name: "privacy", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "privacy")
            self.present(linkingVC, animated: true, completion: nil)
        }
        
        
        @IBAction func help(_ sender: UIButton) {
            let storyboard = UIStoryboard(name: "Help", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "Help")
            self.present(linkingVC, animated: true, completion: nil)
        }
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
        }
        
        
}



