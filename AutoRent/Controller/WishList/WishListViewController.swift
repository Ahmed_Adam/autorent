//
//  WishListViewController.swift
//  AutoRent
//
//  Created by mac on 4/30/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class WishListViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{


    @IBOutlet weak var tableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "wishListCell", for: indexPath) as! wishListTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let cell =  tableView.dequeueReusableCell(withIdentifier: "wishListCell", for: indexPath) as! wishListTableViewCell
        cell.awakeFromNib()
    }
    
    
    

    @IBAction func back(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container")
        self.navigationController?.present(linkingVC, animated: true, completion: nil)
        
        //pushViewController(linkingVC, animated: true)
        
        //
        
    }

}
