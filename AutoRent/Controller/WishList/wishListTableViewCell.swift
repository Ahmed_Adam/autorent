//
//  wishListTableViewCell.swift
//  AutoRent
//
//  Created by mac on 5/2/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class wishListTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var viewLeft: NSLayoutConstraint!
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var carName: UILabel!
    @IBOutlet weak var carConstraint: NSLayoutConstraint!
    @IBOutlet weak var carNameConstraint: NSLayoutConstraint!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe(sender:)))
        swipe.direction = .left
        swipe.numberOfTouchesRequired = 1
        self.addGestureRecognizer(swipe)

    }

    @objc func handleSwipe (sender: UISwipeGestureRecognizer) {
        viewLeft.constant = 10
       carConstraint.constant = 20
       carNameConstraint.constant = 20
       editButton.isHidden = false
       deleteButton.isHidden = false
        
    }


}
