//
//  PaymentViewController.swift
//  AutoRent
//
//  Created by mac on 5/2/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func back(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container2")
        self.navigationController?.present(linkingVC, animated: true, completion: nil)
        //  pushViewController(linkingVC, animated: true)
        
    }

}
