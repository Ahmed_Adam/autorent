//
//  SplashViewController.swift
//  AutoRent
//
//  Created by mac on 4/24/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit
import MediaPlayer
import ChainableAnimations


class SplashViewController: UIViewController  {
    

    @IBOutlet weak var car: UIImageView!
    @IBOutlet weak var circlePic: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var autuRent: UILabel!
    @IBOutlet weak var rent: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        DispatchQueue.main.asyncAfter(deadline: .now() + 6) {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container")
            self.present(linkingVC, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear (_ animated: Bool) {
        super.viewWillAppear(animated)
        self.animateIntro()
        
            
    }
    
    func animateIntro () {
        //let animator = ChainableAniamtor(view: logoImageView)
        self.circlePic.animator.rotate(angle: 360).animateWithRepeat(t: 0.2, count: 20)
        
        self.autuRent.animator.transform(scale: 6).animate(t: 1)
        self.rent.animator.transform(scale: 6).animate(t: 1)
        //move(x: 166.0).thenAfter(t: 1).make(scale: 3.0).spring.animate(t: 3)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            self.car.animator.move(x: 230.0).animate(t: 4)
        }
        
    }
    


}


