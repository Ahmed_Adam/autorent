//
//  PrivacyAndPolicyViewController.swift
//  AutoRent
//
//  Created by mac on 4/30/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class PrivacyAndPolicyViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBOutlet weak var agreeall: UIButton!
    @IBOutlet weak var ch2: UIButton!
    @IBOutlet weak var ch3: UIButton!
    @IBOutlet weak var ch4: UIButton!
    @IBOutlet weak var ch5: UIButton!
    
    @IBAction func agreeAll(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
           agreeall.setImage(#imageLiteral(resourceName: "icons8-checked_checkbox_filled-1"), for: .normal)
        }
        else {
             agreeall.setImage(#imageLiteral(resourceName: "icons8-checked_checkbox"), for: .normal)
        }
        
        
    }
    @IBAction func ch2(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            ch2.setImage(#imageLiteral(resourceName: "icons8-checked_checkbox_filled-1"), for: .normal)
        }
        else {
            ch2.setImage(#imageLiteral(resourceName: "icons8-checked_checkbox"), for: .normal)
        }
    }
    @IBAction func ch3(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            ch3.setImage(#imageLiteral(resourceName: "icons8-checked_checkbox_filled-1"), for: .normal)
        }
        else {
            ch3.setImage(#imageLiteral(resourceName: "icons8-checked_checkbox"), for: .normal)
        }
    }
    @IBAction func ch4(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            ch4.setImage(#imageLiteral(resourceName: "icons8-checked_checkbox_filled-1"), for: .normal)
        }
        else {
            ch4.setImage(#imageLiteral(resourceName: "icons8-checked_checkbox"), for: .normal)
        }
    }
    @IBAction func ch5(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            ch5.setImage(#imageLiteral(resourceName: "icons8-checked_checkbox_filled-1"), for: .normal)
        }
        else {
            ch5.setImage(#imageLiteral(resourceName: "icons8-checked_checkbox"), for: .normal)
        }
    }
    
    
    @IBAction func back(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container")
        self.navigationController?.present(linkingVC, animated: true, completion: nil)
        //  pushViewController(linkingVC, animated: true)
        
    }
}
