//
//  ExtraViewController.swift
//  AutoRent
//
//  Created by mac on 4/25/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class ExtraViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func back(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container2")
        self.navigationController?.present(linkingVC, animated: true, completion: nil)
        
    }

}
