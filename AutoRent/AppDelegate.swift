//
//  AppDelegate.swift
//  AutoRent
//
//  Created by mac on 4/22/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        statusBarStylev()
        UIApplication.shared.statusBarStyle = .lightContent
        return true
    }

    func statusBarStylev (){
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        
       

        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
           // statusBar.backgroundColor = UIColor.white
        }
        UIApplication.shared.statusBarStyle = .lightContent
    }
}

