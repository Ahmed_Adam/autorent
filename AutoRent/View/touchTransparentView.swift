//
//  touchTransparentView.swift
//  AutoRent
//
//  Created by mac on 4/30/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
import UIKit


class TouchTransparentView: UIView {
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return false
    }
}
